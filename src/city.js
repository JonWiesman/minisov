// city.js

class City
{
    constructor(planet, vert, teamColors, initialResources)
    {
        this.planet = planet;
        this.radius = 0.5;
        this.startingVert = vert;
        this.teamColors = teamColors;
        this.color = this.teamColors.normal;
        this.model = new Icosphere(new Vec3(0.0, 0.0, 0.0), 1, this.radius, null, this.color);
        this.model.owner = this;
        this.model.SetMouseOwner = function(poly)
        {
            this.owner.SetMouseOwner(poly);
        }                             
        this.SetPosition(new Vec3(0, 0, 0));

        this.modes = {
            growth: 0,
            spawn: 1,
            research: 2,
        };
        this.growthCost = 100;
        this.spawnCost = 100;
        
        this.currentMode = this.modes.spawn;
        this.resources = initialResources;


        this.ready = false;
    }

    SetPosition(vec)
    {
        this.position = vec;
        this.model.tm = new Matrix4x4(new Vec3(this.radius, 0, 0),
                                new Vec3(0, this.radius, 0),
                                new Vec3(0, 0, this.radius),
                                this.position);
    }

    SetColor(color)
    {
        this.color.r = color.r;
        this.color.g = color.g;
        this.color.b = color.b;
    }

    SetMouseOwner(poly)
    {
        this.containsMouse = true;
    }

    GetSpawnVert()
    {
        for(let i = 0; i < this.spawnVerts.length; i++)
        {
            let areaVert = this.spawnVerts[i];
            if(areaVert.vi == this.vert)
            {
                continue;
            }
            let isLand = this.planet.landSphere.verts[areaVert.vi].LengthSq() > 1;
            if(isLand)
            {
                return areaVert.vi;
            }
        }
        return -1;
    }

    SpawnUnit()
    {
        let vi = this.GetSpawnVert();
        game.AddObject(new Unit(this.planet, vi, this.teamColors));
    }

    GetTriangles()
    {
        this.landArea = {
            allIndices: [],
            verts: this.planet.landSphere.verts,
            colors: [],
            uvs: [],
        };
        let emptyUvs = new Vec3(0.0, 0.0);
        this.landArea.colors.length = this.landArea.verts.length;
        this.landArea.colors.fill(this.teamColors.normal, 0, this.landArea.verts.length);
        this.landArea.uvs.length = this.landArea.verts.length;
        this.landArea.uvs.fill(emptyUvs, 0, this.landArea.verts.length);
        let landSphere = this.planet.landSphere;
        for(let i = 0; i < landSphere.indices.length; i += 3)
        {
            let ai = landSphere.indices[i];
            let bi = landSphere.indices[i + 1];
            let ci = landSphere.indices[i + 2];
            if(this.vertMap.has(ai) && this.vertMap.has(bi) && this.vertMap.has(ci))
            {
                this.landArea.allIndices.push(ai);
                this.landArea.allIndices.push(bi);
                this.landArea.allIndices.push(ci);
                if(landSphere.verts[ai].LengthSq() > 1 || landSphere.verts[bi].LengthSq() > 1 || landSphere.verts[ci].LengthSq() > 1)
                {
                }
            }
        }
        this.landArea.tm = this.planet.landSphere.tm;
        this.landArea.indices = this.landArea.allIndices;
    }

    Update(dt)
    {
        if(!this.ready)
        {
            if(this.planet.phase == this.planet.phases.complete)
            {
                this.vert = this.startingVert;
                let loc = this.planet.GetVertLocation(this.vert).worldLoc;
                this.SetPosition(loc);

                this.vertMap = new Map();
                this.planet.FillVertsAroundVert(this.planet.landSphere, this.vert, 4, this.vertMap);
                this.spawnVerts = [];
                this.vertMap.forEach((v, k, m) => {
                    if(4 - v > 0)
                    {
                        this.spawnVerts.push({vi: k, dist: 4 - v});
                    }
                });
                this.spawnVerts.sort((a, b) =>{
                    return a.dist - b.dist;
                });
                this.GetTriangles();

                this.planet.ClearFog(this.vert);
                this.ready = true;
            }
            return;
        }
        if(this.resources >= this.spawnCost && this.currentMode == this.modes.spawn)
        {
            this.SpawnUnit();
            this.resources -= this.spawnCost;
        }

    }

    Render(mgl)
    {
        mgl.RenderObject(this.model);
        if(this.landArea)
        {
            mgl.RenderObject(this.landArea);
        }
    }

}