// unit.js

class Unit 
{
    constructor(planet, vert, teamColors)
    {
        this.planet = planet;
        this.radius = 0.5;
        this.teamColors = teamColors;
        this.color = new Color(0, 1, 1);
        this.model = new Cube(new Vec3(0.0, 0.0, 0.0), this.radius, null, this.color);
        this.model.owner = this;
        this.model.SetMouseOwner = function(poly)
        {
            this.owner.SetMouseOwner(poly);
        }                             
        this.SetPosition(new Vec3(0, 0, 0));
        this.startingVert = vert;
        this.ready = false;
        this.speed = 10;
        this.landSpeed = 2;
        this.waterSpeed = 4;
        this.shallowSpeed = 0.5;
        this.boatThreshold = planet.mediumWater;
    }

    SetPosition(vec)
    {
        this.position = vec;
        this.model.tm = new Matrix4x4(new Vec3(this.radius, 0, 0),
                                new Vec3(0, this.radius, 0),
                                new Vec3(0, 0, this.radius),
                                this.position);
    }

    SetColor(color)
    {
        this.color.r = color.r;
        this.color.g = color.g;
        this.color.b = color.b;
    }

    SetMouseOwner(poly)
    {
        this.containsMouse = true;
    }

    UpdateNextVert()
    {
        let vertMap = new Map();
        if(this.vert != this.nextVert)
        {
            vertMap.set(this.vert, this.vert);
            vertMap.set(this.nextVert, this.nextVert);
        }
        else
        {
            this.planet.FillVertsAroundVert(this.planet.landSphere, this.vert, 1, vertMap);
        }
        let closestVert = -1;
        let closestDist = 10000000;
        let closestPos = null;
        vertMap.forEach((v, k, m) => {
            let pos = this.planet.GetVertLocation(k).worldLoc;
            let dist = pos.Sub(this.destLoc).LengthSq();
            if(dist < closestDist)
            {
                closestDist = dist;
                closestVert = k;
                closestPos = pos;
            }
        });
        this.nextVert = closestVert;
        this.nextLoc = closestPos;

        // check conversion
        if(this.vert != this.nextVert)
        {
            let vertLandHeight = this.planet.landSphere.verts[this.vert].Length();
            let nextLandHeight = this.planet.landSphere.verts[this.nextVert].Length();
            let waterSource = vertLandHeight < 1;
            let waterDest = nextLandHeight < 1;
    
            let convert = false;
            if((waterSource && waterDest && !this.boat) || (waterSource && !waterDest && this.boat))
            {
                this.converting = true;
                this.conversionTime = 1;
            }
        }
    }

    GetCurrentHeight()
    {
        let vertLandHeight =  this.planet.landSphere.verts[this.vert].Length();
        polyInfo = `vertLandHeight = ${vertLandHeight.toFixed(3)}`;
        if(this.vert == this.nextVert)
        {
            return vertLandHeight;
        }
        let vertPos = this.planet.GetVertLocation(this.vert).worldLoc;
        let nextVertPos = this.planet.GetVertLocation(this.nextVert).worldLoc;
        let nextLandHeight = this.planet.landSphere.verts[this.nextVert].Length();
        polyInfo += ` nextLandHeight = ${nextLandHeight.toFixed(3)}`;
        let vdiff = this.position.Sub(vertPos).Length();
        let tdiff = vertPos.Sub(nextVertPos).Length();

        let vpct = (vdiff / tdiff);
        let curHeight = vertLandHeight * (1 - vpct) + nextLandHeight * vpct;

        return curHeight;
    }

    Update(dt)
    {
        if(!this.ready)
        {
            if(this.planet.phase == this.planet.phases.complete)
            {
                this.vert = this.startingVert;
                this.nextVert = this.vert;
                this.destVert = this.vert;
                this.nextLoc = this.planet.GetVertLocation(this.vert).worldLoc;
                this.SetPosition(this.nextLoc);

                this.planet.ClearFog(this.vert);
                this.ready = true;
            }
            return;
        }

        polyInfo = '';
        let height = this.GetCurrentHeight();
        if(this.boat === undefined)
        {
            this.boat = height < 1;
        }
        this.speed = (this.boat ? this.waterSpeed : this.landSpeed);

        let outsideThresh = Math.abs(1 - height) > this.boatThreshold;
        if(this.converting)
        {
            this.conversionTime -= dt;
            polyInfo += ` converting to ${this.boat ? 'army' : 'boat'}. height = ${height.toFixed(3)}. Time left = ${this.conversionTime.toFixed(2)}`;
            if(this.conversionTime <= 0)
            {
                this.converting = false;
                this.boat = !this.boat;
            }
        }
        else 
        {
            if(!this.nextLoc.IsEqual(this.position))
            {
                let d = this.nextLoc.Sub(this.position);
                if(d.LengthSq() < dt * this.speed * dt * this.speed)
                {
                    this.SetPosition(this.nextLoc);
                    this.vert = this.nextVert;
                    this.planet.ClearFog(this.vert);
                    this.UpdateNextVert();
                }
                else
                {
                    d.SetLength(dt * this.speed);
                    let pos = this.position.Add(d);
                    this.SetPosition(pos);
                }
            }
        }

        if(input.keysJustPressed['g'] && game.selectedObject == this)
        {
            this.destVert = this.planet.mouseOverVert;
            this.destLoc = this.planet.GetVertLocation(this.destVert).worldLoc;
            this.UpdateNextVert();
        }
        if(input.isNewTouch && this.containsMouse)
        {
            game.SetSelectedObject(this);
        }

        if(game.selectedObject == this)
        {
            this.SetColor(this.teamColors.selected);
        }
        else if(this.containsMouse)
        {
            this.SetColor(this.teamColors.highlight);
        }
        else
        {
            this.SetColor(this.teamColors.normal);
        }
        this.containsMouse = false;
    }

    Render(mgl)
    {
        mgl.RenderObject(this.model);
    }
}