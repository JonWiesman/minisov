class IcosphereGraphNode
{
    constructor(idx, vert)
    {
        this.idx = idx;
        this.vert = vert;
        this.n = new Map();
        this.polys = [];
    }
    addNeighbor(node)
    {
        this.n.set(node.idx, node);
    }
    addPoly(index)
    {
        this.polys.push(index);
    }
}

class Icosphere
{
    constructor(center, radius, numSubdivisions, texture, color)
    {
        // http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
        let t = (1.0 + Math.sqrt(5.0)) / 2.0;

        this.verts = [];
        this.verts.push(new Vec3(-1.0, t, 0.0));
        this.verts.push(new Vec3(1.0, t, 0.0));
        this.verts.push(new Vec3(-1.0, -t, 0.0));
        this.verts.push(new Vec3(1.0, -t, 0.0));

        this.verts.push(new Vec3(0.0, -1.0, t));
        this.verts.push(new Vec3(0.0, 1.0, t));
        this.verts.push(new Vec3(0.0, -1.0, -t));
        this.verts.push(new Vec3(0.0, 1.0, -t));

        this.verts.push(new Vec3(t, 0.0, -1.0));
        this.verts.push(new Vec3(t, 0.0, 1.0));
        this.verts.push(new Vec3(-t, 0.0, -1.0));
        this.verts.push(new Vec3(-t, 0.0, 1.0));

        this.indices = [];
        this.indices.push(0); this.indices.push(11); this.indices.push(5);
        this.indices.push(0); this.indices.push(5); this.indices.push(1);
        this.indices.push(0); this.indices.push(1); this.indices.push(7);
        this.indices.push(0); this.indices.push(7); this.indices.push(10);
        this.indices.push(0); this.indices.push(10); this.indices.push(11);

        this.indices.push(1); this.indices.push(5); this.indices.push(9);
        this.indices.push(5); this.indices.push(11); this.indices.push(4);
        this.indices.push(11); this.indices.push(10); this.indices.push(2);
        this.indices.push(10); this.indices.push(7); this.indices.push(6);
        this.indices.push(7); this.indices.push(1); this.indices.push(8);

        this.indices.push(3); this.indices.push(9); this.indices.push(4);
        this.indices.push(3); this.indices.push(4); this.indices.push(2);
        this.indices.push(3); this.indices.push(2); this.indices.push(6);
        this.indices.push(3); this.indices.push(6); this.indices.push(8);
        this.indices.push(3); this.indices.push(8); this.indices.push(9);

        this.indices.push(4); this.indices.push(9); this.indices.push(5);
        this.indices.push(2); this.indices.push(4); this.indices.push(11);
        this.indices.push(6); this.indices.push(2); this.indices.push(10);
        this.indices.push(8); this.indices.push(6); this.indices.push(7);
        this.indices.push(9); this.indices.push(8); this.indices.push(1);
        this.subdivisions = [];
        this.subdivisions.push(this.indices);

        this.colors = [];

        this.NormalizeAll();
        for (let i = 0; i < numSubdivisions; i++)
        {
            this.Subdivide();
        }

        // I don't know how to texture icosphere yet - just assume solid color texture for now
        this.uvs = [];
        for (let i = 0; i < this.verts.length; i++)
        {
            this.uvs.push(new Vec3(0.0, 0.0));
            if(color === undefined)
            {
                this.colors.push(new Color(1, 1, 1));
            }
            else
            {
                //this.colors.push(new Color(color.r, color.g, color.b));
                this.colors.push(color);
            }
            
        }

        this.texture = texture;

        this.center = center;
        this.tm = new Matrix4x4(new Vec3(radius, 0, 0),
                                new Vec3(0, radius, 0),
                                new Vec3(0, 0, radius),
                                center);
    }

    NormalizeAll()
    {
        let newVerts = [];
        for(let i = 0; i < this.verts.length; i++)
        {
            newVerts.push(this.verts[i].Normalize());
        }
        this.verts = newVerts;
    }

    getKey(i, j)
    {
        if(i < j)
        {
            return (i * 1000000) + j;
        }
        return (j * 1000000) + i;
    }

    getGraph()
    {
        if(this.graph === undefined)
        {
            this.makeGraph();
        }
        return this.graph;
    }

    makeGraph()
    {
        let graph = [];
        for(let i = 0; i < this.verts.length; i++)
        {
            graph.push(new IcosphereGraphNode(i, this.verts[i]));
        }
        for(let i = 0; i < this.indices.length; i += 3)
        {
            let ai = this.indices[i];
            let bi = this.indices[i + 1];
            let ci = this.indices[i + 2];
            let a = graph[ai];
            let b = graph[bi];
            let c = graph[ci];

            a.addNeighbor(b);
            a.addNeighbor(c);
            a.addPoly(i);
            b.addNeighbor(a);
            b.addNeighbor(b);
            b.addPoly(i);
            c.addNeighbor(a);
            c.addNeighbor(b);
            c.addPoly(i);

        }
        this.graph = graph;
    }

    Subdivide()
    {
        let newIndices = [];
        let indexMap = new Map();
        for (let i = 0; i < this.indices.length; i += 3)
        {
            let ai = this.indices[i];
            let bi = this.indices[i + 1];
            let ci = this.indices[i + 2];
            let a = this.verts[ai];                     // newVerts.length
            let b = this.verts[bi];                     // newVerts.length + 1
            let c = this.verts[ci];                     // newVerts.length + 2
            let ab = a.Add(b).Scale(0.5);               // newVerts.length + 3
            let bc = b.Add(c).Scale(0.5);               // newVerts.length + 4
            let ca = c.Add(a).Scale(0.5);               // newVerts.length + 5

            let abi = -1;
            let abKey = this.getKey(ai, bi);
            if(indexMap.has(abKey))
            {
                abi = indexMap.get(abKey);
            }
            else
            {
                this.verts.push(ab.Normalize());
                abi = this.verts.length - 1;
                indexMap.set(abKey, abi);
            }

            let bci = -1;
            let bcKey = this.getKey(bi, ci);
            if(indexMap.has(bcKey))
            {
                bci = indexMap.get(bcKey);
            }
            else
            {
                this.verts.push(bc.Normalize());
                bci = this.verts.length - 1;
                indexMap.set(bcKey, bci);
            }

            let cai = -1;
            let caKey = this.getKey(ci, ai);
            if(indexMap.has(caKey))
            {
                cai = indexMap.get(caKey);
            }
            else
            {
                this.verts.push(ca.Normalize());
                cai = this.verts.length - 1;
                indexMap.set(caKey, cai);
            }

            newIndices.push(ai); newIndices.push(abi); newIndices.push(cai);    // top
            newIndices.push(abi); newIndices.push(bi); newIndices.push(bci);    // bottom left
            newIndices.push(cai); newIndices.push(bci); newIndices.push(ci);    // bottom right
            newIndices.push(abi); newIndices.push(bci); newIndices.push(cai);   // middle
        }
        this.indices = newIndices;
        this.subdivisions.push(this.indices);
        delete this.graph;
        this.edgeLength = this.verts[this.indices[0]].Sub(this.verts[this.indices[1]]).Length();
    }

    Deform(magnitude, vi)
    {
        if(vi === undefined)
        {
            vi = Math.floor(Math.random() * this.verts.length);
        }
        let vert = this.verts[vi];

        for(let i = 0; i < this.verts.length; i++)
        {
            let d = 1;
            if(i != vi)
            {
                d = 1 + this.verts[i].Sub(vert).Length() / this.edgeLength;
            }
            if(d > 10)
            {
                continue;
            }
            let m = magnitude / d;
            let vd = this.verts[i].Length();
            let newHeight = Math.max(this.minHeight, Math.min(this.maxHeight, vd + m));

            this.verts[i].SetLength(newHeight);
            this.OnVertHeightChange(i, newHeight);
        }
    }

    OnVertHeightChange(i, newHeight)
    {

    }
}

class TerraSphere extends Icosphere
{
    constructor(center, radius, numSubdivisions, texture, color, waterSphere)
    {
        super(center, radius, numSubdivisions, texture, color);
        this.waterSphere = waterSphere;

        this.iceShelfThresholdY = 0.95;
        this.polarSnowLineY = 0.935;
        
        this.maxHeight = 1.05;
        this.minHeight = 0.95;
        this.mountainSnowHeight = 1.045;
        this.vegetationHeight = 1;
        this.iceShelfHeight = 1.001;

        this.sandColor = new Color(219/255, 147/255, 95/255);
        this.green = new Color(0, 0.5, 14/255);
        this.ice = new Color(192/255, 255/255, 1);
        this.waterIce = new Color(200/255, 200/255, 1);


    }

    Deform(magnitude)
    {
        let vi = Math.floor(Math.random() * this.verts.length);
        let vert = this.verts[vi];

        for(let i = 0; i < this.verts.length; i++)
        {
            let d = 1;
            if(i != vi)
            {
                d = 1 + this.verts[i].Sub(vert).Length() / this.edgeLength;
            }
            if(d > 10)
            {
                continue;
            }
            let m = magnitude / d;
            let vd = this.verts[i].Length();
            let newHeight = Math.max(this.minHeight, Math.min(this.maxHeight, vd + m));

            this.verts[i].SetLength(newHeight);
            this.OnVertHeightChange(i, newHeight);
        }
    }

    OnVertHeightChange(i, newHeight)
    {
        let v = this.verts[i];

        if(v.y < -this.iceShelfThresholdY || v.y > this.iceShelfThresholdY)
        {
            this.waterSphere.verts[i].SetLength(this.iceShelfHeight);
        }
        if(v.y < -this.polarSnowLineY || v.y >= this.polarSnowLineY)
        {
            if(newHeight > 1)
            {
                this.colors[i] = this.ice;
            }
            else
            {
                this.colors[i] = this.sandColor;
            }
            this.waterSphere.colors[i] = this.waterIce;
        }
        else if(newHeight < this.vegetationHeight)
        {
            // sand
            this.colors[i] = this.sandColor;
        }
        else if(newHeight < this.mountainSnowHeight)
        {
            // green
            this.colors[i] = this.green; 
        }
        else
        {
            // ice
            this.colors[i] = this.ice;
        }
    }
}