class Polygon
{
    constructor(paWorld, pbWorld, pcWorld, paScreen, pbScreen, pcScreen, paUV, pbUV, pcUV, normal, 
        texture, aColor, bColor, cColor, owner, polyIndex)
    {
        this.paWorld = paWorld;
        this.pbWorld = pbWorld;
        this.pcWorld = pcWorld;
        this.paScreen = paScreen;
        this.pbScreen = pbScreen;
        this.pcScreen = pcScreen;
        this.paUV = paUV;
        this.pbUV = pbUV;
        this.pcUV = pcUV;
        this.normal = normal;
        this.texture = texture;
        this.paColor = aColor;
        this.pbColor = bColor;
        this.pcColor = cColor;
        this.owner = owner;
        this.polyIndex = polyIndex;

        // Cache average z-value for use in poly sorting
        this.zAvg = (paWorld.z + pbWorld.z + pcWorld.z) * 0.3333;
    }
}