class Scene
{
    constructor(mgl, input)
    {
        this.mgl = mgl;
        this.input = input;

        // this.texBlue = new Texture("./textures/blue.png");
        // this.texSand = new Texture("./textures/sand.png");
        // this.texWhite = new Texture("./textures/white.png");
        
        this.cameraPos = new Vec3(0, 0, 0);
        this.cameraTarget = new Vec3(0, 0, 0);
        this.cameraUp = new Vec3(0, 1, 0);
        this.cameraAngleH = 180.0;
        this.cameraAngleV = 60.0;
        this.cameraDistance = 150.0;
        this.cameraMinDistance = 40.0;
        this.cameraMaxDistance = 300.0;
        this.cameraRotateSpeed = 0.003125;
        this.cameraPanSpeed = 0.1;
        this.cameraMinAngleV = -90.0;
        this.cameraMaxAngleV = 90.0;
        this.mouseWheelSpeed = 0.1;

        this.mgl.lights.push(new Light(new Vec3(-1.0, 0.0, 0.0), new Color(0.3, 0.3, 0.3), new Color(1.0, 1.0, 1.0)));
        this.mgl.lights.push(new Light(new Vec3(0.0, -1.0, 0.0), new Color(0.3, 0.3, 0.3), new Color(1.0, 1.0, 1.0)));

        // this.Reset();
    }

    // Reset()
    // {
    //     this.planet = new Planet();
    //     this.unit1 = new Unit(this.planet, 0, new Color(0.5, 0.5, 0), new Color(0.75, 0.75, 0.25), new Color(1, 1, 1));
    //     this.unit2 = new Unit(this.planet, 1, new Color(0.5, 0, 0), new Color(0.75, 0, 0), new Color(1, 0, 0));
    //     this.objects = [];
    //     this.objects.push(this.planet);
    //     this.objects.push(this.unit1);
    //     this.objects.push(this.unit2);
    // }

    Update(dt)
    {
        // if(this.input.keysJustPressed['r'])
        // {
        //     this.Reset();
        // }
        this.UpdateCamera(dt);
        // this.objects.forEach(obj => obj.Update(dt));
    }

    UpdateCamera(dt)
    {
        let cameraRotateSpeed = this.cameraRotateSpeed * this.cameraDistance;
        // Rotate based on mouse/touch movement
        if (this.input.isTouchActive)
        {
            this.cameraAngleH += this.input.dx * cameraRotateSpeed;
            this.cameraAngleV = Math.max(Math.min(this.cameraAngleV + this.input.dy * cameraRotateSpeed,
                                                  this.cameraMaxAngleV),
                                                  this.cameraMinAngleV);
        }
        
        // Move in/out based on mouse wheel
        this.cameraDistance = Math.max(Math.min(this.cameraDistance + this.input.wheel*this.mouseWheelSpeed,
                                                this.cameraMaxDistance),
                                                this.cameraMinDistance);

        // Position camera                     
        let cameraDistanceH = Math.cos(this.cameraAngleV*Math.PI/180.0) * this.cameraDistance;
        let cameraOffset = new Vec3(Math.cos(this.cameraAngleH*Math.PI/180.0) * cameraDistanceH,
                                    Math.sin(this.cameraAngleV*Math.PI/180.0) * this.cameraDistance,
                                    Math.sin(this.cameraAngleH*Math.PI/180.0) * cameraDistanceH);

        this.cameraPos = this.cameraTarget.Add(cameraOffset);
        this.mgl.SetCameraLookAt(this.cameraPos.Add(new Vec3(0.0, 0.0, 0.0)),
                                 this.cameraTarget.Add(new Vec3(0.0, 0.0, 0.0)),
                                 this.cameraUp);
    }

    Render()
    {
        //this.objects.forEach(obj => obj.Render(this.mgl));
    }
}