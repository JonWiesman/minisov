// Planet.js

class Planet 
{
    constructor()
    {
        this.maxHeight = 1.05;
        this.minHeight = 0.95;
        this.sphereRadius = 30.0;

        this.deepWater = 0.005;
        this.mediumWater = 0.0025;
        this.emptyUV = new Vec3(0, 0, 0);

        this.waterColor = new Color(0, 0, 0.8);
        this.mediumWaterColor = new Color(54/255, 36/255, 177/255);
        this.shallowWaterColor = new Color(109/255, 73/255, 149/255);
        this.sandColor = new Color(219/255, 147/255, 95/255);
        this.green = new Color(0, 0.5, 14/255);
        this.ice = new Color(192/255, 255/255, 1);
        this.waterIce = new Color(200/255, 200/255, 1);
        this.selectionColor = new Color(1, 0, 0);
        this.fogColors = [
            new Color(0.4, 0.4, 0.4),
            new Color(0.45, 0.45, 0.45),
            new Color(0.5, 0.5, 0.5),
            new Color(0.55, 0.55, 0.55),
            new Color(0.60, 0.60, 0.60),
            new Color(0.65, 0.65, 0.65),
        ];

        this.waterHeight = this.sphereRadius * 0.96;
        this.maxWaterHeight = this.sphereRadius * 1.0067;
        this.waterSphere = new Icosphere(new Vec3(0.0, 0.0, 0.0), this.waterHeight, 5, null, this.waterColor);
        this.landSphere = new Icosphere(new Vec3(0.0, 0.0, 0.0), this.sphereRadius, 5, null, new Color(0.5, 0, 0));
        this.fogSphere = new Icosphere(new Vec3(0, 0, 0), this.sphereRadius, 5, null, this.fogColors[0]);
        this.landSphere.maxHeight = this.maxHeight;
        this.landSphere.minHeight = this.minHeight;

        this.iceShelfThresholdY = 0.95;
        this.polarSnowLineY = 0.935;
        
        this.mountainSnowHeight = 1.03;
        this.vegetationHeight = 1;
        this.iceShelfHeight = 1.001;

        this.drawWater = true;
        this.drawLand = true;
        this.drawFog = true;

        this.currentLod = this.landSphere.subdivisions.length - 1;


        this.lavaColors = [
            new Color(91/255, 34/255, 51/255),
            new Color(164/255, 31/255, 0),
            new Color(230/255, 44/255, 6/255),
            new Color(1, 25/255, 5/255),
            new Color(1, 130/255, 19/255),
            new Color(254/255, 174/255, 1/255),
            new Color(249/255, 208/255, 102/255),
        ];

        this.phases = {
            lava: 0,
            deforming: 1,
            cooling: 2,
            waterCollection: 3,
            freezing: 4,
            life: 5,
            complete: 6,
        }
        this.phaseUpdates = [
            10, 60, 60, 30, 30, 30, 0
        ]

        this.InitializeLava();

        this.phase = this.phases.lava;
        this.phaseCount = 0;

        this.tm = this.landSphere.tm;
    }

    InitializeFog()
    {
        for(let i = 0; i < this.fogSphere.verts.length; i++)
        {
            this.fogSphere.verts[i].SetLength(1.0001);
            let r = Math.floor(Math.random() * this.fogColors.length);
            this.fogSphere.colors[i] = this.fogColors[r];
        }
    }

    UpdateFog()
    {
        for(let i = 0; i < 10; i++)
        {
            let v = Math.floor(Math.random() * this.fogSphere.verts.length);
            let r = Math.floor(Math.random() * this.fogColors.length);
            this.fogSphere.colors[v] = this.fogColors[r];
        }
    }

    InitializeLava()
    {
        this.lavaIdx = [];
        for(let i = 0; i < this.landSphere.verts.length; i++)
        {
            let idx = Math.floor(Math.random() * this.lavaColors.length);
            this.lavaIdx.push(idx);
            this.landSphere.colors[i] = this.lavaColors[idx];
        }
    }
    UpdateLava(coolChance, deform)
    {
        for(let i = 0; i < 50; i++)
        {
            let vi = Math.floor(Math.random() * this.lavaIdx.length);
            {
                let r = Math.random();
                if(r < coolChance)
                {
                    this.lavaIdx[vi] = Math.max(0, this.lavaIdx[vi] - 1);
                }
                else
                {
                    this.lavaIdx[vi] = Math.min(this.lavaColors.length - 1, this.lavaIdx[vi] + 1);
                }
                this.landSphere.colors[vi] = this.lavaColors[this.lavaIdx[vi]];
            }
            if(deform)
            {
                let r = Math.random();
                if(r >= 0.45)
                {
                    // asteroid hit
                    let m = Math.random() * -0.0125;
                    this.landSphere.Deform(m);
                }
                else if(r >= 0.44)
                {
                    vi = Math.floor(Math.random() * 12);
                    let m = Math.random() * 0.0135;
                    this.landSphere.Deform(m, vi);
                }
                else
                {
                    // volcano?
                    let m = Math.random() * 0.0135;
                    this.landSphere.Deform(m);
                }
            }
        }
    }

    CoolLava()
    {
        let updatesPerCooling = Math.floor(this.phaseUpdates[this.phase] / this.lavaColors.length);
        for(let vi = 0; vi < this.lavaIdx.length; vi++)
        {
            if(vi % updatesPerCooling != this.phaseCount % updatesPerCooling)
            {
                continue;
            }
            this.lavaIdx[vi] = Math.max(0, this.lavaIdx[vi] - 1);
            this.landSphere.colors[vi] = this.lavaColors[this.lavaIdx[vi]];
        }
    }

    Update(dt)
    {
        if(input.keysJustPressed['f'])
        {
            this.drawFog = !this.drawFog;
            this.mustCombine = true;
        }
        if(input.keysJustPressed['w'])
        {
            this.drawWater = !this.drawWater;
            this.mustCombine = true;
        }
        if(input.keysJustPressed['l'])
        {
            this.drawLand = !this.drawLand;
            this.mustCombine = true;
        }
        if(input.keysJustPressed['-'])
        {
            this.currentLod = Math.max(0, this.currentLod - 1);
            this.mustCombine = true;
        }
        if(input.keysJustPressed['='])
        {
            this.currentLod = Math.min(this.landSphere.subdivisions.length - 1, this.currentLod + 1);
            this.mustCombine = true;
        }

        this.phaseCount++;
        if(this.phase == this.phases.complete)
        {
            this.UpdateFog();
            return;
        }
        if(this.phaseCount == this.phaseUpdates[this.phase])
        {
            this.SetPhase(this.phase + 1);
            this.phaseCount = 0;
            
        }
        if(this.phase == this.phases.lava)
        {
            this.UpdateLava(0.5, false);
        }
        else if(this.phase == this.phases.deforming)
        {
            this.UpdateLava(0.98, true);
        }
        else if(this.phase == this.phases.cooling)
        {
            this.CoolLava();
        }
        else if(this.phase == this.phases.waterCollection)
        {
            this.UpdateWaterRise();
        }
        else if(this.phase == this.phases.freezing)
        {
            if(this.waterHeight > this.sphereRadius)
            {
                this.SetWaterHeight(Math.max(this.sphereRadius, this.waterHeight - 0.0125));
            }
            this.Freeze();
        }
        else if(this.phase == this.phases.life)
        {
            this.SpreadLife();
        }
    }

    SpreadLife()
    {
        let graph = this.landSphere.getGraph();
        if(this.spreadLifeFrame === undefined)
        {
            this.spreadLifeFrame = [];
        }
        for(let i = 0; i < graph.length; i++)
        {
            let vert = graph[i].vert;
            let vi = graph[i].idx;
            if(vert.LengthSq() < 1)
            {
                continue;
            }
            if(this.landSphere.colors[vi] == this.green)
            {
                // already life here
                continue;
            }
            if(this.landSphere.colors[vi] == this.ice)
            {
                // no life can grow here
                continue;
            }
            graph[i].n.forEach(node =>{
                if(node.vert.LengthSq() < 1)
                {
                    // our neighbor is underwater
                    if(Math.random() < 0.125)
                    {
                        this.landSphere.colors[vi] = this.green;
                        this.spreadLifeFrame[vi] = this.phaseCount;
                    }
                }
                if(this.landSphere.colors[node.idx] == this.green)
                {
                    // our neighbor has life
                    if(this.phaseCount > this.spreadLifeFrame[node.idx])
                    {
                        if(Math.random() < 0.085)
                        {
                            this.landSphere.colors[vi] = this.green;
                            this.spreadLifeFrame[vi] = this.phaseCount;
                        }
                    }
                }
            });
        }
    }

    Freeze()
    {
        let freezeProgress = this.phaseCount / this.phaseUpdates[this.phase];
        let iceLine = (1 - (1 - this.iceShelfThresholdY) * freezeProgress);
        let snowLine = (1 - (1 - this.polarSnowLineY) * freezeProgress);
        let mtnSnowLine = (this.maxHeight - (this.maxHeight - this.mountainSnowHeight) * freezeProgress);
        for(let i = 0; i < this.polarIndices.length; i++)
        {
            let vi = this.polarIndices[i];
            let landVert = this.landSphere.verts[vi];
            let y = landVert.y;
            if(Math.abs(y) > iceLine)
            {
                this.waterSphere.colors[vi] = this.waterIce;
            }
            if(Math.abs(y) > snowLine)
            {
                this.landSphere.colors[vi] = this.ice;
            }
        }
        for(let i = 0; i < this.tallPeaks.length; i++)
        {
            let vi = this.tallPeaks[i];
            let landVert = this.landSphere.verts[vi];
            let lengthSq = landVert.LengthSq();
            if(lengthSq > mtnSnowLine * mtnSnowLine)
            {
                this.landSphere.colors[vi] = this.ice;
            }
        }
    }

    SetPhase(phase)
    {
        this.phase = phase;
        if(this.phase == this.phases.freezing)
        {
            // create beaches and list of tall peaks
            this.tallPeaks = [];
            this.polarIndices = [];
            for(let i = 0; i < this.landSphere.verts.length; i++)
            {
                let v = this.landSphere.verts[i];
                let lengthSq = v.LengthSq();
                if(lengthSq < 1.0)
                {
                    this.landSphere.colors[i] = this.sandColor;
                }
                if(lengthSq > this.mountainSnowHeight * this.mountainSnowHeight)
                {
                    this.tallPeaks.push(i);
                }
                if(Math.abs(v.y) > this.iceShelfThresholdY)
                {
                    this.polarIndices.push(i);
                }
            }
            console.log(`tallPeaks count = ${this.tallPeaks.length}`);
        }
        if(this.phase == this.phases.complete)
        {
            this.CombineSpheres();
            this.InitializeFog();
            let teamColors = [
                {normal: new Color(0.5, 0.5, 0), highlight: new Color(0.75, 0.75, 0.25), selected: new Color(1, 1, 1)},
                {normal: new Color(0.5, 0, 0), highlight: new Color(0.75, 0, 0.25), selected: new Color(1, 0, 0)},
                {normal: new Color(0, 0.5, 0.5), highlight: new Color(0, 0.75, 0.75), selected: new Color(0, 1, 1)},
            ];
            let team = 0;
            for(let i = 0; i < 12 && team < teamColors.length; i++)
            {
                if(this.landSphere.verts[i].LengthSq() > 1)
                {
                    let city = new City(this, i, teamColors[team], 100);
                    game.AddObject(city);
                    team++;
                }
            }
        }
    }

    SetWaterHeight(height)
    {
        this.waterHeight = height;
        this.waterSphere.tm = new Matrix4x4(new Vec3(this.waterHeight, 0, 0),
                                new Vec3(0, this.waterHeight, 0),
                                new Vec3(0, 0, this.waterHeight),
                                this.waterSphere.center);

        let waterFactor = height / this.sphereRadius;                                
        for(let i = 0; i < this.waterSphere.verts.length; i++)
        {
            let depth = waterFactor - this.landSphere.verts[i].Length();
            if(depth > this.deepWater)
            {
                this.waterSphere.colors[i] = this.waterColor;
            }
            else if(depth > this.mediumWater)
            {
                this.waterSphere.colors[i] = this.mediumWaterColor;
            }
            else if(depth > 0)
            {
                this.waterSphere.colors[i] = this.shallowWaterColor;
            }
            else
            {
                this.waterSphere.colors[i] = this.sandColor;
            }
        }
    }

    UpdateWaterRise()
    {
        if(this.waterHeight < this.maxWaterHeight)
        {
            this.SetWaterHeight(Math.min(this.maxWaterHeight, this.waterHeight + 0.05));
        }
    }

    CombineSpheres()
    {
        let landVertCount = this.landSphere.verts.length;
        if(this.combinedSphere === undefined)
        {
            this.combinedSphere = {
                verts: [],
                indices: [],
                colors: [],
                uvs: [],
                owner: this,
            };
    
            // copy all the verts from all 3 spheres
            for(let i = 0; i < landVertCount; i++)
            {
                this.combinedSphere.verts[i] = this.landSphere.verts[i];
                this.combinedSphere.colors[i] = this.landSphere.colors[i];
                this.combinedSphere.uvs[i] = this.landSphere.uvs[i];
    
                this.combinedSphere.verts[i + landVertCount] = this.waterSphere.verts[i];
                this.combinedSphere.colors[i + landVertCount] = this.waterSphere.colors[i];
                this.combinedSphere.uvs[i + landVertCount] = this.waterSphere.uvs[i];
    
                this.combinedSphere.verts[i + 2 * landVertCount] = this.fogSphere.verts[i];
                this.combinedSphere.colors[i + 2 * landVertCount] = this.fogSphere.colors[i];
                this.combinedSphere.uvs[i + 2 * landVertCount] = this.fogSphere.uvs[i];
            }
        }

        this.combinedSphere.indices = [];

        let indices = this.landSphere.subdivisions[this.currentLod];
        for(let i = 0; i < indices.length; i+=3)
        {
            let ai = indices[i];
            let bi = indices[i + 1];
            let ci = indices[i + 2];
            let wai = ai + landVertCount;
            let wbi = bi + landVertCount;
            let wci = ci + landVertCount;
            let fai = ai + landVertCount * 2;
            let fbi = bi + landVertCount * 2;
            let fci = ci + landVertCount * 2;

            let la = this.landSphere.verts[ai].LengthSq();
            let lb = this.landSphere.verts[bi].LengthSq();
            let lc = this.landSphere.verts[ci].LengthSq();

            let fa = this.fogSphere.verts[ai].LengthSq();
            let fb = this.fogSphere.verts[bi].LengthSq();
            let fc = this.fogSphere.verts[ci].LengthSq();

            if(fa < 1 || fb < 1 || fc < 1)
            {
                if(this.drawWater && (la < 1 || lb < 1 || lc < 1))
                {
                    // push water tri (we may push both!)
                    this.combinedSphere.indices.push(wai);
                    this.combinedSphere.indices.push(wbi);
                    this.combinedSphere.indices.push(wci);
                }
                if(this.drawLand && (la >= 1 || lb >= 1 || lc >= 1))
                {
                    // push land tri (we may push 1, 2, or 3 !)
                    this.combinedSphere.indices.push(ai);
                    this.combinedSphere.indices.push(bi);
                    this.combinedSphere.indices.push(ci);
                }
            }
            if(this.drawFog && (fa >= 1 || fb >= 1 || fc >= 1))
            {
                this.combinedSphere.indices.push(fai);
                this.combinedSphere.indices.push(fbi);
                this.combinedSphere.indices.push(fci);
            }
        }

        this.combinedSphere.tm = new Matrix4x4(new Vec3(this.sphereRadius, 0, 0),
                                               new Vec3(0, this.sphereRadius, 0),
                                               new Vec3(0, 0, this.sphereRadius),
                                               this.waterSphere.center);
        this.combinedSphere.SetMouseOwner = function(poly)
        {
            this.owner.SetMouseOwner(poly);
        }                                            
        this.mustCombine = false;
    }

    GetCombinedOverVert(index)
    {
        let r = {};
        let landVertCount = this.landSphere.verts.length;
        index %= landVertCount;

        if(this.fogSphere.verts[index].LengthSq() > 1)
        {
            r.vert = this.fogSphere.verts[index].Copy();
            r.color = this.fogSphere.colors[index];
        }
        else
        {
            let lsq = this.landSphere.verts[index].LengthSq();
            let wsq = this.waterSphere.verts[index].LengthSq();
            if(lsq > wsq)
            {
                r.vert = this.landSphere.verts[index].Copy();
                r.color = this.landSphere.colors[index];
            }
            else
            {
                r.vert = this.waterSphere.verts[index].Copy();
                r.color = this.waterSphere.colors[index];
            }
        }
        r.vert.SetLength(r.vert.Length() + 0.001);
        return r;
    }

    MakePolyGraph()
    {
        let edgeMap = new Map();
        this.polygraph = [];
        for(let i = 0; i < this.landSphere.indices.length; i+=3)
        {
            let ai = this.landSphere.indices[i];
            let bi = this.landSphere.indices[i + 1];
            let ci = this.landSphere.indices[i + 2];
            let a = this.landSphere.verts[ai];
            let b = this.landSphere.verts[bi];
            let c = this.landSphere.verts[ci];
            
            let center = a.Add(b).Add(c).Scale(0.33333);

            let abKey = this.landSphere.getKey(ai, bi);
            let bcKey = this.landSphere.getKey(bi, ci);
            let caKey = this.landSphere.getKey(ci, ai);

            let node = {
                polyIndex: i,
                center: center,
                neighbors: [],
                polys: [],
            };
            this.polygraph.push(node);

            let keys = [abKey, bcKey, caKey];
            for(let k = 0; k < 3; k++)
            {
                if(edgeMap.has(keys[k]))
                {
                    let other = edgeMap.get(keys[k]);
                    node.neighbors.push(other.node);
                    other.node.neighbors[other.edgeIdx] = node;
                }
                else
                {
                    node.neighbors.push(null);
                    edgeMap.set(keys[k], {
                        node: node,
                        edgeIdx: k,
                    });
                }
            }
        }
    }

    GetMouseLengthSqToVert(vec)
    {
        let dx = vec.x - input.canvasx;
        dx *= dx;
        let dy = vec.y - input.canvasy;
        dy *= dy;
        return dx + dy;
    }

    SetMouseOwner(poly)
    {
        let index = poly.polyIndex;
        let landVertCount = this.landSphere.verts.length;

        let asq = this.GetMouseLengthSqToVert(poly.paScreen);
        let bsq = this.GetMouseLengthSqToVert(poly.pbScreen);
        let csq = this.GetMouseLengthSqToVert(poly.pcScreen);

        let closestVert = index;
        if(bsq < asq)
        {
            if(bsq < csq)
            {
                closestVert = index + 1;
            }
            else
            {
                closestVert = index + 2;
            }
        }
        else if(csq < asq)
        {
            closestVert = index + 2;
        }

        let vi = this.combinedSphere.indices[closestVert];
        let csv = this.combinedSphere.verts[vi];
        //polyInfo = `${csv.x.toFixed(2)}, ${csv.y.toFixed(2)}, ${csv.z.toFixed(2)}. `;
        if(vi >= landVertCount * 3)
        {
            return;
        }
        this.mouseOverVert = vi % landVertCount;
        csv = this.combinedSphere.verts[this.mouseOverVert];
        //polyInfo += `mouse = ${csv.x.toFixed(2)}, ${csv.y.toFixed(2)}, ${csv.z.toFixed(2)}`;

        let graph = this.landSphere.getGraph();
        let combinedVertCount = landVertCount * 3;
        
        vi = vi % landVertCount;

        this.mouseHex = {
            verts: [],
            indices: [],
            colors: [],
            uvs: [],
            tm: this.combinedSphere.tm,
        };
        for(let i = 0; i < graph[vi].polys.length; i++)
        {
            let node = graph[vi];
            let idx = node.polys[i];
            let ai = this.landSphere.indices[idx];
            let bi = this.landSphere.indices[idx + 1];
            let ci = this.landSphere.indices[idx + 2];
            
            let ov = this.GetCombinedOverVert(ai);
            this.mouseHex.verts[i * 3] = ov.vert;
            this.mouseHex.colors[i * 3] = this.selectionColor.Blend(ov.color, 0.25);
            this.mouseHex.uvs[i * 3] = this.emptyUV;
    
            ov = this.GetCombinedOverVert(bi);
            this.mouseHex.verts[i * 3 + 1] = ov.vert;
            this.mouseHex.colors[i * 3 + 1] = this.selectionColor.Blend(ov.color, 0.25);
            this.mouseHex.uvs[i * 3 + 1] = this.emptyUV;
    
            ov = this.GetCombinedOverVert(ci);
            this.mouseHex.verts[i * 3 + 2] = ov.vert;
            this.mouseHex.colors[i * 3 + 2] = this.selectionColor.Blend(ov.color, 0.25);
            this.mouseHex.uvs[i * 3 + 2] = this.emptyUV;
    
            this.mouseHex.indices[i * 3] = i * 3;
            this.mouseHex.indices[i * 3 + 1] = i * 3 + 1;
            this.mouseHex.indices[i * 3 + 2] = i * 3 + 2;
        }
    }

    FillVertsAroundVert(sphere, vi, range, vertMap)
    {
        this.visits++;
        vertMap.set(vi, range);
        if(range == 0)
        {
            return;
        }
        let graph = sphere.getGraph();
        graph[vi].n.forEach((v, k, m) => {
            if(!vertMap.has(k) || vertMap.get(k) < range - 1)
            {
                this.FillVertsAroundVert(sphere, k, range - 1, vertMap);
            }
        });
    }

    DumpVertMap(vertMap)
    {
        console.log(`visits = ${this.visits}`);
        let rangeCounts = [];
        vertMap.forEach((v, k, m) => {
            if(rangeCounts[v] === undefined)
            {
                rangeCounts[v] = 0;
            }
            rangeCounts[v]++;
        });
        for(let i = 0; i < rangeCounts.length; i++)
        {
            console.log(`${rangeCounts[i]} verts with range ${i}`);
        }
    }

    ClearFog(vi, depth)
    {
        this.visits = 0;
        if(depth === undefined)
        {
            depth = 7;
        }
        if(vi >= this.fogSphere.verts.length)
        {
            vi -= this.fogSphere.verts.length;
        }
        let vertMap = new Map();
        this.FillVertsAroundVert(this.fogSphere, vi, depth, vertMap);

        vertMap.forEach((v, k, m) => {
            if(this.fogSphere.verts[k].LengthSq() > 1)
            {
                if(v > 0)
                {
                    this.fogSphere.verts[k].SetLength(0.5);
                }
                else
                {
                    this.fogSphere.verts[k].SetLength(Math.max(1.0001, this.landSphere.verts[k].Length() + 0.0001));
                }
                this.mustCombine = true;
            }
        });
    }

    Render(mgl)
    {
        if(this.phase <= this.phases.life)
        {
            mgl.RenderObject(this.landSphere);
        }
        if(this.phase >= this.phases.waterCollection && this.phase <= this.phases.life)
        {
            mgl.RenderObject(this.waterSphere);
        }
        if(this.phase >= this.phases.complete)
        {
            if(this.mustCombine)
            {
                this.CombineSpheres();
            }
            mgl.RenderObject(this.combinedSphere);
            //mgl.RenderObject(this.fogSphere);
        }
        if(this.mouseHex)
        {
            mgl.RenderObject(this.mouseHex);
        }
    }

    GetVertLocation(i)
    {
        if(this.combinedSphere === undefined)
        {
            return {
                worldLoc: new Vec3(0, 0, 0),
                onLand: false,
                revealed: false,
            };
        }
        i = i % this.landSphere.verts.length;
        let land = this.landSphere.verts[i];
        let water = this.waterSphere.verts[i];
        let fog = this.fogSphere.verts[i];

        let location = {
            revealed: (fog.LengthSq() < 1),
        };
        if(land.LengthSq() >= 1)
        {
            location.worldLoc = land.Scale(this.sphereRadius);
            location.onLand = true;
        }
        else
        {
            location.worldLoc = water.Scale(this.sphereRadius);
            location.onLand = false;
        }
        return location;
    }
}