// game.js

class Game
{
    constructor()
    {
        this.scene = new Scene(mgl, input);
        this.input = input;
        this.mgl = mgl;
        this.Reset();

        this.selectedObject = null;
    }

    Reset()
    {
        this.planet = new Planet();
        this.objects = [];
        this.AddObject(this.planet);
        // let city1 = new City(this.planet, 0, new Color(0.5, 0.5, 0), new Color(0.75, 0.75, 0.25), new Color(1, 1, 1), 100);
        // let city2 = new City(this.planet, 2, new Color(0.5, 0.0, 0), new Color(0.75, 0, 0.25), new Color(1, 0, 0), 100);
        // let unit1 = new Unit(this.planet, 0, new Color(0.5, 0.5, 0), new Color(0.75, 0.75, 0.25), new Color(1, 1, 1));
        // let unit2 = new Unit(this.planet, 1, new Color(0.5, 0, 0), new Color(0.75, 0, 0), new Color(1, 0, 0));
        // this.AddObject(this.planet);
        // this.AddObject(city1);
        // this.AddObject(city2);
        // this.AddObject(unit1);
        // this.AddObject(unit2);
    }

    AddObject(o)
    {
        this.objects.push(o);
    }

    Update(dt)
    {
        if(this.input.keysJustPressed['r'])
        {
            this.Reset();
        }
        this.scene.Update(dt);
        this.objects.forEach(obj => obj.Update(dt));
    }

    Render(dt)
    {
        this.objects.forEach(obj => obj.Render(this.mgl));
    }

    SetSelectedObject(object)
    {
        this.selectedObject = object;
    }
}